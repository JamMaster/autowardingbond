local superOnDamage = {};
local superOnEffect = {};

function onInit()
	superOnDamage = ActionDamage.onDamage; -- Stash original away
	ActionDamage.onDamage = onDamage;
	ActionsManager.registerResultHandler("damage", onDamage);

	superOnEffect = ActionEffect.onEffect; -- Stash original away
	ActionEffect.onEffect = onEffect;
	ActionsManager.registerResultHandler("effect", onEffect);

	-- Add the effect to the spell
	DataSpell.parsedata["warding bond"] = getWardingBondEffect();
end

function getWardingBondEffect()
	local rEffect = { type = "effect", sName = "Warding Bond; AC: 1; SAVE: 1; RESIST: all", nDuration = 1, sUnits = "hour" }

	return { rEffect };
end

function getWardingBondFinalEffects(sCasterName, sTargetName, nDuration, sUnits)
	local casterEffect = { type = "effect", sName = "Warding Bond (Caster): " .. sTargetName, sTargeting = "self", nDuration = nDuration, sUnits = sUnits };
	local targetEffect = { type = "effect", sName = "Warding Bond (Receiver): " .. sCasterName .. "; AC: 1; SAVE: 1; RESIST: all", nDuration = nDuration, sUnits = sUnits };

	return casterEffect, targetEffect
end

function onEffect(rSource, rTarget, rRoll)
	if StringManager.startsWith(rRoll.sDesc, "[EFFECT] Warding Bond") then
		-- Debug.console("Found warding bond effect: ", rRoll);
		local rEffect = EffectManager.decodeEffectFromText(rRoll.sDesc, rRoll.bSecret);

		-- Debug.console("rSource: ", rSource);
		-- Debug.console("rTarget: ", rTarget);
		-- Not allowed without a target and self
		if rTarget == nil or rTarget.sCTNode == rSource.sCTNode then
			return;
		end
		-- TODO: have a lookup table for the actual actors in case there are multiple charactes with the same name
		local rCasterEffect, rTargetEffect = getWardingBondFinalEffects(rSource.sName, rTarget.sName, rEffect.nDuration, rEffect.sUnits);
		local rCasterRoll, rTargetRoll = EffectManager.encodeEffect(rCasterEffect), EffectManager.encodeEffect(rTargetEffect);

		superOnEffect(rSource, rTarget, rTargetRoll);
		superOnEffect(rSource, rSource, rCasterRoll);
	else
		superOnEffect(rSource, rTarget, rRoll);
	end
end

-- https://www.sageadvice.eu/is-warding-bond-the-source-of-its-generated-damage-instance/
-- https://www.sageadvice.eu/warding-bond/
function onDamage(rSource, rTarget, rRoll)
	local sWardingCaster = getWardingBondReceiver(rTarget);
	if sWardingCaster == nil or sWardingCaster == "" then
		superOnDamage(rSource, rTarget, rRoll);
		return;
	end

	-- Debug.console("The warding caster is: ", sWardingCaster);
	
	local rWardingActor = nil;
	for _,v in pairs(CombatManager.getCombatantNodes()) do
		if DB.getValue(v, "name", "") == sWardingCaster then
			rWardingActor = ActorManager.resolveActor(v);
		end
	end
	
	if rWardingActor == nil then
		sendChatMessage(rTarget, rWardingActor, "Warning: The warding caster is not in the combat tracker!");
		superOnDamage(rSource, rTarget, rRoll);
		return;
	end
	
	-- Debug.console("Distance between actors is: ", distanceBetween(rTarget, rWardingActor));

	if distanceBetween(rTarget, rWardingActor) > 60 then
		sendChatMessage(rTarget, rWardingActor, "The warding bond is broken due to distance");
		
		EffectManager.removeEffect(ActorManager.getCTNode(rTarget), "% *[Ww]arding% +[Bb]ond% *");
		EffectManager.removeEffect(ActorManager.getCTNode(rWardingActor), "% *[Ww]arding% +[Bb]ond% *");

		superOnDamage(rSource, rTarget, rRoll);
		return;
	end

	-- Apply the damage after potentially removing the bond effect
	superOnDamage(rSource, rTarget, rRoll);
	
	-- Debug.console("The warder actor is : ", rWardingActor);
	-- Debug.console("The original roll is : ", rRoll);

	-- Calculate the damage received
	local nTotal = ActionsManager.total(rRoll);
	local rFinalReceivedDamage = getFinalReceivedDamage(rSource, rTarget, (tonumber(rRoll.bSecret) == 1), rRoll.sDesc, nTotal);
	if rFinalReceivedDamage == nil then
		return;
	end
	-- Debug.console("The final damages are: ", rFinalReceivedDamage);

	-- Apply the same damage to the caster
	-- Technically the source is the original source, but this here only serves as a placeholder for the regex to work
	local sWardReceivedDamage = "[DAMAGE] Warding Bond Transfer ";
	local nTotalWardReceivedDamage = 0;
	for type, dmg in pairs(rFinalReceivedDamage) do
		-- TODO: Maybe there is something better than 0d0?
		sWardReceivedDamage = rFinalReceivedDamage[type] .. "[TYPE: " .. type .. " (0d0=" .. dmg .. ")]";
		nTotalWardReceivedDamage = nTotalWardReceivedDamage + dmg;
	end

	ActionDamage.applyDamage(rSource, rWardingActor, (tonumber(rRoll.bSecret) == 1), sWardReceivedDamage, nTotalWardReceivedDamage);
	return;
end

function sendChatMessage(rSource, rTarget, text)
	local msg = { font = "msgfont", text = text, icon = "portrait_gm_token" };
	
	ActionsManager.messageResult(false, rSource, rTarget, msg, msg);
end

-- Returns how much damage the warded actor received after all the complicated bonuses, so it can be applied to the caster
-- Based on ActionDamage.applyDamage
function getFinalReceivedDamage(rSource, rTarget, bSecret, sDamage, nTotal)
	local sTargetNodeType, nodeTarget = ActorManager.getTypeAndNode(rTarget);
	if not nodeTarget then
		return nil;
	end

	-- Decode damage description
	local rDamageOutput = ActionDamage.decodeDamageText(nTotal, sDamage);
	
	-- Apply any targeted damage effects 
	if rSource and rTarget and rTarget.nOrder then
		ActionDamage.applyTargetedDmgEffectsToDamageOutput(rDamageOutput, rSource, rTarget);
		ActionDamage.applyTargetedDmgTypeEffectsToDamageOutput(rDamageOutput, rSource, rTarget);
	end
	
	-- Handle avoidance/evasion and half damage
	local isAvoided = false;
	local isHalf = string.match(sDamage, "%[HALF%]");
	local sAttack = string.match(sDamage, "%[DAMAGE[^]]*%] ([^[]+)");
	if sAttack then
		local sDamageState = ActionDamage.getDamageState(rSource, rTarget, StringManager.trim(sAttack));
		if sDamageState == "none" then
			isAvoided = true;
		elseif sDamageState == "half_success" then
			isHalf = true;
		elseif sDamageState == "half_failure" then
			isHalf = true;
		end
	end
	if isAvoided then
		for kType, nType in pairs(rDamageOutput.aDamageTypes) do
			rDamageOutput.aDamageTypes[kType] = 0;
		end
		rDamageOutput.nVal = 0;
	elseif isHalf then
		local bCarry = false;
		for kType, nType in pairs(rDamageOutput.aDamageTypes) do
			local nOddCheck = nType % 2;
			rDamageOutput.aDamageTypes[kType] = math.floor(nType / 2);
			if nOddCheck == 1 then
				if bCarry then
					rDamageOutput.aDamageTypes[kType] = rDamageOutput.aDamageTypes[kType] + 1;
					bCarry = false;
				else
					bCarry = true;
				end
			end
		end
		rDamageOutput.nVal = math.max(math.floor(rDamageOutput.nVal / 2), 1);
	end
	
	-- Apply damage type adjustments
	local rReductions = getDamageAdjust(rSource, rTarget, rDamageOutput.nVal, rDamageOutput);

	local rTypedTakenDamage = {};
	for k, v in pairs(rDamageOutput.aDamageTypes) do
		if rReductions[k] then
			rTypedTakenDamage[k] = v + rReductions[k];
		end
	end

	return rTypedTakenDamage;
end

-- Based on ActionDamage.getDamageAdjust
function getDamageAdjust(rSource, rTarget, nDamage, rDamageOutput)
	-- Get damage adjustment effects
	local aImmune = ActionDamage.getReductionType(rSource, rTarget, "IMMUNE");
	local aVuln = ActionDamage.getReductionType(rSource, rTarget, "VULN");
	local aResist = ActionDamage.getReductionType(rSource, rTarget, "RESIST");
	
	-- Handle immune all
	if aImmune["all"] then
		local rReductions = {};
		for k, v in pairs(rDamageOutput.aDamageTypes) do
			rReductions[k] = -v;
		end
		return rReductions;
	end
	
	-- Iterate through damage type entries for vulnerability, resistance and immunity
	local nDamageAdjust = 0;
	local bResistCarry = false;
	local rReductions = {};
	for k, v in pairs(rDamageOutput.aDamageTypes) do
		rReductions[k] = 0;
		
		-- Get individual damage types for each damage clause
		local aSrcDmgClauseTypes = {};
		local aTemp = StringManager.split(k, ",", true);
		for _,vType in ipairs(aTemp) do
			if vType ~= "untyped" and vType ~= "" then
				table.insert(aSrcDmgClauseTypes, vType);
			end
		end
		
		-- Handle standard immunity, vulnerability and resistance
		local bLocalVulnerable = ActionDamage.checkReductionType(aVuln, aSrcDmgClauseTypes);
		local bLocalResist = ActionDamage.checkReductionType(aResist, aSrcDmgClauseTypes);
		local bLocalImmune = ActionDamage.checkReductionType(aImmune, aSrcDmgClauseTypes);
		
		-- Calculate adjustment
		-- Vulnerability = double
		-- Resistance = half
		-- Immunity = none
		local nLocalDamageAdjust = 0;
		if bLocalImmune then
			rReductions[k] = -v;
			nLocalDamageAdjust = -v;
		else
			-- Handle numerical resistance
			local nLocalResist = ActionDamage.checkNumericalReductionType(aResist, aSrcDmgClauseTypes, v);
			if nLocalResist ~= 0 then
				rReductions[k] = rReductions[k] - nLocalResist;
				nLocalDamageAdjust = nLocalDamageAdjust - nLocalResist;
			end
			-- Handle numerical vulnerability
			local nLocalVulnerable = ActionDamage.checkNumericalReductionType(aVuln, aSrcDmgClauseTypes);
			if nLocalVulnerable ~= 0 then
				rReductions[k] = rReductions[k] - nLocalVulnerable;
				nLocalDamageAdjust = nLocalDamageAdjust + nLocalVulnerable;
			end
			-- Handle standard resistance
			if bLocalResist then
				local nResistOddCheck = (nLocalDamageAdjust + v) % 2;
				local nAdj = math.ceil((nLocalDamageAdjust + v) / 2);
				rReductions[k] = rReductions[k] - nAdj;
				nLocalDamageAdjust = nLocalDamageAdjust - nAdj;
				if nResistOddCheck == 1 then
					if bResistCarry then
						rReductions[k] = rReductions[k] + 1;
						nLocalDamageAdjust = nLocalDamageAdjust + 1;
						bResistCarry = false;
					else
						bResistCarry = true;
					end
				end
			end
			-- Handle standard vulnerability
			if bLocalVulnerable then
				rReductions[k] = rReductions[k] + (nLocalDamageAdjust + v);
				nLocalDamageAdjust = nLocalDamageAdjust + (nLocalDamageAdjust + v);
			end
		end
		
		-- Apply adjustment to this damage type clause
		nDamageAdjust = nDamageAdjust + nLocalDamageAdjust;
	end
	
	-- Handle damage threshold
	local nDTMod, nDTCount = EffectManager5E.getEffectsBonus(rTarget, {"DT"}, true);
	if nDTMod > 0 then
		if nDTMod > (nDamage + nDamageAdjust) then 
			nDamageAdjust = -nDamage;
			for k, v in pairs(rReductions) do
				rReductions[k] = -v;
			end
		end
	end

	return rReductions;
end

-- If protected by warding bond, it returns the name of the caster, otherwise nil
-- Based on EffectManager.hasEffect
function getWardingBondReceiver(rActor)
	if not rActor then
		return nil;
	end
	
	-- Iterate through each effect
	local tResults = {};
	for _, v in pairs(ActorManager.getEffects(rActor)) do
		local nActive = DB.getValue(v, "isactive", 0);
		if nActive == 1 then
			local sLabel = DB.getValue(v, "label", "");
			local aEffectComps = EffectManager.parseEffect(sLabel);

			for kEffectComp, sEffectComp in ipairs(aEffectComps) do
				local name, value = string.gmatch(sEffectComp, "(.*)% *:% *(.*)")();
				if name ~= nil and StringManager.trim(name):lower() == "warding bond (receiver)" and value ~= nil then
					return value;
				end
			end
		end
	end

	return nil;
end

function distanceBetween(rSource, rTarget)
	if rSource == nil or rTarget == nil then
		return -1;
	end
	local srcToken, srcImage = getTokenAndImage(rSource);
    local tgtToken, tgtImage = getTokenAndImage(rTarget);

	if srcToken == nil or tgtToken == nil then
		return -1;
	end

	if srcImage.getDistanceBetween == nil then
		return -1; --This function is only available in FGU.
	end

	return srcImage.getDistanceBetween(srcToken, tgtToken);
end

function getTokenAndImage(rActor)
	if rActor == nil then
		return nil, nil;
	end
	
	local token = CombatManager.getTokenFromCT(rActor.sCTNode);
	local image = ImageManager.getImageControl(token);
	if image == nil then
		return nil, nil;
	else
		return token, image;
	end
end